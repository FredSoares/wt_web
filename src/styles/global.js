import { createGlobalStyle } from 'styled-components';
import 'react-toastify/dist/ReactToastify.css';

export default createGlobalStyle`
 @import url('https://fonts.googleapis.com/css?family=Roboto:400,700&display=swap');
  * {
    margin: 0;
    padding: 0;
    outline: 0;
    box-sizing: border-box;
  }
  *:focus {
    outline: 0;
  }
  html {
    font-size: 62.5%;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
  }
  html, body, #root {
    height: 100%;

  }
  body {
    -webkit-font-smoothing: antialiased;
  }
  body, input, button, textarea {
    font: 1.4rem 'Roboto', sans-serif;
  }
  a {
    text-decoration: none;
  }
  ul {
    list-style: none;
  }
  button {
    cursor: pointer;
  }
  select {
      -webkit-appearance: none;
      -moz-appearance: none;
      appearance: none;
  }

  textarea{
    resize: none;
  }
`;
