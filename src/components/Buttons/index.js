import React from 'react';
import PropTypes from 'prop-types';

import { Container } from './styles';

export default function Button({
  background,
  color,
  children,
  onClick,
  ...rest
}) {
  return (
    <Container
      onClick={onClick}
      {...rest}
      color={color}
      background={background}
    >
      {children}
    </Container>
  );
}

Button.propTypes = {
  children: PropTypes.string,
  color: PropTypes.string,
  background: PropTypes.string,
  onClick: PropTypes.func,
};

Button.defaultProps = {
  children: '',
  color: '#FFFFFF',
  background: '#73D888',
  onClick: () => {},
};
