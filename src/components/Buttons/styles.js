import styled from 'styled-components';

export const Container = styled.button`
  display: flex;
  justify-content: center;
  width: auto;
  box-shadow: 0rem 1.5rem 3.5rem rgba(3, 3, 3, 0.1);
  height: 4.4rem;
  border: 0;
  border-radius: 0.3rem;
  font-style: normal;
  font-weight: 500;
  font-size: 1.6rem;
  line-height: 2.4rem;
  color: ${props => props.color};
  cursor: pointer;
  transition: background 0.2s;
  padding: 1.2rem 1.4rem;
  transition: 180ms ease-in-out;
  background-color: ${props => props.background};
  :hover {
    transform: scale(1.01);
  }
  :active {
    transform: scale(0.99);
  }
  :disabled {
    opacity: 0.5;
    cursor: not-allowed;
  }
`;
