import styled from 'styled-components';

export const Container = styled.div`
  max-width: 60rem;
  margin: 5rem auto;
  padding: 1rem;

  button {
    margin-top: 1rem;
  }

  form {
    display: flex;
    flex-direction: column;
    margin-top: 3rem;
    input {
      background: rgba(0, 0, 0, 0.1);
      border: 0;
      border-radius: 0.4rem;
      height: 4.4rem;
      padding: 0 1.5rem;
      margin: 0 0 1rem;
      &::placeholder {
        color: rgba(255, 255, 255, 0.7);
      }
    }
    span {
      color: #fb6f91;
      align-self: flex-start;
      margin: 0 0 1rem;
      font-weight: bold;
    }
    hr {
      border: 0;
      height: 0.1rem;
      background: rgba(255, 255, 255, 0.4);
      margin: 1rem 0 2rem;
    }
  }
`;
