import React, { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Form, Input } from '@rocketseat/unform';

import {
  signOut,
  deleteAccountRequest,
} from '../../store/modules/auth/actions';
import { updateProfileRequest } from '../../store/modules/user/actions';

import Button from '../../components/Buttons';
import { Container } from './styles';

export default function Profile() {
  const dispatch = useDispatch();
  const profile = useSelector(state => state.user.profile);

  const handleSubmit = useCallback(
    data => {
      dispatch(updateProfileRequest(data));
    },
    [dispatch]
  );

  const handleSignOut = useCallback(() => {
    dispatch(signOut());
  }, [dispatch]);

  const handleDeleteAccount = useCallback(() => {
    dispatch(deleteAccountRequest());
  }, [dispatch]);

  return (
    <Container>
      <Form initialData={profile} onSubmit={handleSubmit}>
        <Input name="first_name" placeholder="First name" />
        <Input name="last_name" placeholder="Last name" />
        <Input name="email" type="email" placeholder="Your email" />
        <hr />

        <Input
          name="oldPassword"
          type="password"
          placeholder="Current password"
        />
        <Input name="password" type="password" placeholder="New password" />
        <Input
          name="confirmPassword"
          type="password"
          placeholder="Confirm password"
        />

        <Button type="submit" background="#253f56">
          Update profile
        </Button>

        <Button type="button" onClick={handleSignOut} background="#f64c75">
          Log Out
        </Button>

        <Button
          type="button"
          onClick={handleDeleteAccount}
          background="#96abd1"
        >
          Delete Account
        </Button>
      </Form>
    </Container>
  );
}
