import styled from 'styled-components';

export const Wrapper = styled.div`
  min-height: 100%;
  background: #a7bfe8;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Content = styled.div`
  width: 100%;
  max-width: 31.5rem;
  text-align: center;
  form {
    display: flex;
    flex-direction: column;
    margin-top: 3rem;
    input {
      background: rgba(0, 0, 0, 0.1);
      border: 0;
      border-radius: 0.4rem;
      height: 4.4rem;
      padding: 0 1.5rem;
      margin: 0 0 1rem;
      &::placeholder {
        color: rgba(255, 255, 255, 0.7);
      }
    }
    span {
      color: #fb6f91;
      align-self: flex-start;
      margin: 0 0 1rem;
      font-weight: bold;
    }

    a {
      color: #fff;
      margin-top: 1.5rem;
      font-size: 1.6rem;
      opacity: 0.9;
      &:hover {
        opacity: 1;
      }
    }
  }
`;
