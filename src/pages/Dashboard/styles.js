import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 60rem;
  margin: 5rem auto;

  h1 {
    color: #555;
    font-size: 2.4rem;
    margin: 0 1.5rem;
    align-self: center;
  }

  ul {
    display: flex;
    flex-direction: column;
    margin-top: 3rem;
  }
`;

export const User = styled.li`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 2rem;
  border-radius: 0.4rem;
  background: #e3e3e3;
  margin: 1rem;
  cursor: pointer;
  transition: 180ms ease-in-out;
  :hover {
    transform: scale(1.01);
  }

  strong {
    color: #2e547a;
    font-size: 2.1rem;
    font-weight: 'bold';
  }

  span {
    color: #4f6276;
    margin-top: 0.3rem;
    font-weight: 'normal';
  }
`;
