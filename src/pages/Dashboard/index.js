import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import api from '../../services/api';

import { Container, User } from './styles';

function Dashboard() {
  const token = useSelector(state => state.auth.token);
  const [users, setUsers] = useState([]);

  useEffect(() => {
    async function loadUsers() {
      const response = await api.get('/v1/users', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      setUsers(response.data);
    }

    loadUsers();
  }, [token]);

  return (
    <Container>
      <h1>All Users</h1>
      <ul>
        {users.map(user => (
          <User key={user.id}>
            <strong>
              {user.first_name} {user.last_name}
            </strong>
            <span>{user.email}</span>
          </User>
        ))}
      </ul>
    </Container>
  );
}

export default Dashboard;
