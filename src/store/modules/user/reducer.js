import * as actionTypes from './actionTypes';
import {
  SIGN_OUT,
  SIGN_IN_SUCCESS,
  DELETE_ACCOUNT_SUCCESS,
} from '../auth/actionTypes';

const loadProfile = localStorage.getItem('profile');

const INITIAL_STATE = {
  profile: loadProfile ? JSON.parse(loadProfile) : null,
};

export default function user(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SIGN_IN_SUCCESS:
    case actionTypes.UPDATE_PROFILE_SUCCESS:
      localStorage.setItem('profile', JSON.stringify(action.payload.user));
      return {
        ...state,
        profile: action.payload.user,
      };
    case SIGN_OUT:
    case DELETE_ACCOUNT_SUCCESS:
      localStorage.removeItem('profile');
      return { ...state, profile: null };

    default:
      return state;
  }
}
