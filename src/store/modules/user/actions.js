import * as actionTypes from './actionTypes';

export function updateProfileRequest(data) {
  return {
    type: actionTypes.UPDATE_PROFILE_REQUEST,
    payload: { data },
  };
}

export function updateProfileSuccess({ user }) {
  return {
    type: actionTypes.UPDATE_PROFILE_SUCCESS,
    payload: { user },
  };
}

export function updateProfileFailure() {
  return {
    type: actionTypes.UPDATE_PROFILE_FAILURE,
  };
}
