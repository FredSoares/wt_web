import { takeLatest, call, put, all, select } from 'redux-saga/effects';
import { toast } from 'react-toastify';
import api from '../../../services/api';
import { updateProfileSuccess, updateProfileFailure } from './actions';

export function* updateProfile({ payload }) {
  try {
    // get user token
    const token = yield select(state => state.auth.token);

    const { first_name, last_name, email, ...rest } = payload.data;

    // It will join two objects
    const profile = {
      first_name,
      last_name,
      email,
      ...(rest.oldPassword ? rest : {}),
    };

    const response = yield call(api.patch, '/v1/users', profile, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    toast.success('Profile updated successfully');

    yield put(updateProfileSuccess(response.data));
  } catch (err) {
    toast.error('Error: profile updating failure, check your data.');
    yield put(updateProfileFailure());
  }
}

export default all([takeLatest('@user/UPDATE_PROFILE_REQUEST', updateProfile)]);
