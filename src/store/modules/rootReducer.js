import { combineReducers } from 'redux';

/* import for all reducers */
import auth from './auth/reducer';
import user from './user/reducer';

/* export object with all reducers */
export default combineReducers({
  auth,
  user,
});
