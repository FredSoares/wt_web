import * as actionTypes from './actionTypes';

const INITIAL_STATE = {
  token: localStorage.getItem('token') || null,
  loading: false,
};

export default function auth(state = INITIAL_STATE, action) {
  switch (action.type) {
    case actionTypes.SIGN_IN_REQUEST:
      return { ...state, loading: true };

    case actionTypes.SIGN_IN_SUCCESS:
      localStorage.setItem('token', action.payload.token);

      return {
        ...state,
        token: action.payload.token,
        loading: false,
      };

    case actionTypes.SIGN_FAILURE:
      localStorage.removeItem('token');
      return { ...state, token: null, loading: false };

    case actionTypes.SIGN_OUT:
    case actionTypes.DELETE_ACCOUNT_SUCCESS:
      localStorage.removeItem('token');
      return { ...state, token: null };

    default:
      return state;
  }
}
