export const SIGN_UP_REQUEST = '@auth/SIGN_UP_REQUEST';
export const SIGN_IN_REQUEST = '@auth/SIGN_IN_REQUEST';
export const SIGN_IN_SUCCESS = '@auth/SIGN_IN_SUCCESS';
export const DELETE_ACCOUNT = '@auth/DELETE_ACCOUNT';
export const DELETE_ACCOUNT_SUCCESS = '@auth/DELETE_ACCOUNT_SUCCESS';
export const SIGN_FAILURE = '@auth/SIGN_FAILURE';
export const SIGN_OUT = '@auth/SIGN_OUT';
