import * as actionTypes from './actionTypes';

export function signInRequest(email, password) {
  return {
    type: actionTypes.SIGN_IN_REQUEST,
    payload: { email, password },
  };
}

export function signInSuccess(token, user) {
  return {
    type: actionTypes.SIGN_IN_SUCCESS,
    payload: { token, user },
  };
}

export function signUpRequest(first_name, last_name, email, password) {
  return {
    type: actionTypes.SIGN_UP_REQUEST,
    payload: { first_name, last_name, email, password },
  };
}

export function deleteAccountRequest() {
  return {
    type: actionTypes.DELETE_ACCOUNT,
  };
}

export function deleteAccontSuccess() {
  return {
    type: actionTypes.DELETE_ACCOUNT_SUCCESS,
  };
}

export function signFailure() {
  return {
    type: actionTypes.SIGN_FAILURE,
  };
}

export function signOut() {
  return {
    type: actionTypes.SIGN_OUT,
  };
}
