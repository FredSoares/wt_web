import { takeLatest, call, put, all, select } from 'redux-saga/effects';

import { toast } from 'react-toastify';
import api from '../../../services/api';
import history from '../../../services/history';

import * as actionTypes from './actionTypes';
import { signInSuccess, signFailure, deleteAccontSuccess } from './actions';

export function* signIn({ payload }) {
  try {
    const { email, password } = payload;

    // request for session
    const response = yield call(api.post, 'v1/session', {
      email,
      password,
    });

    // get data from response
    const { token, user } = response.data;

    // call signIn actions success
    yield put(signInSuccess(token, user));

    // redirect to dashboard
    history.push('/dashboard');
  } catch (err) {
    // show erro message
    toast.error('Authentication failed check your data');
    // call actions fail
    yield put(signFailure());
  }
}

export function* signUp({ payload }) {
  try {
    const { first_name, last_name, email, password } = payload;
    yield call(api.post, '/v1/users', {
      first_name,
      last_name,
      email,
      password,
    });

    history.push('/');
  } catch (err) {
    toast.error('Verify your information and try again.');

    yield put(signFailure());
  }
}

export function* deleteAccount() {
  try {
    // get user token
    const token = yield select(state => state.auth.token);

    yield call(api.delete, '/v1/users', {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    toast.success('Account deleted successfully');
    // call delete account actions success
    yield put(deleteAccontSuccess());
    history.push('/');
  } catch (err) {
    toast.error('Error to delete Account');
  }
}

// redirect user to root after log out
export function* signOut() {
  // redirect to home page
  yield history.push('/');
}

// export all saga
export default all([
  takeLatest(actionTypes.SIGN_IN_REQUEST, signIn),
  takeLatest('@auth/SIGN_UP_REQUEST', signUp),
  takeLatest(actionTypes.SIGN_OUT, signOut),
  takeLatest(actionTypes.DELETE_ACCOUNT, deleteAccount),
]);
